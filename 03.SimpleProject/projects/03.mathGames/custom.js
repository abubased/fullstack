// স্টার্ট বাটন
let btn = document.getElementById("btn");
// টাইম রিমেনিং বক্স
let timego = document.getElementById("timego");
//স্কোর বক্স
let scoreBox = document.getElementById("score");
let score = 0;
// প্রশ্ন
let question = document.getElementById("question");
// কারেক্ট বক্স
let right = document.getElementById("right");
// ট্রাই এ্যাগেইন বক্স
let wrong = document.getElementById("wrong");
// সঠীক উত্তরটি
let correctAnswer;

// স্টার্ট বাটনে ক্লিক করলে গেইম শুরু হবে
btn.addEventListener("click", startGame);

function startGame() {
  // যদি বাটনের ভিতর Start Game লেখা থাকে তাহলে
  if (btn.innerHTML == "Start Game") {
    // স্টার্ট বাটনকে রিসেট বাটনে পরিণত করবো
    btn.innerHTML = "Reset Game";
    // স্কোর বক্স ভিসিবল হবে
    visible("scoreBox");
    // টাইম রিমেনিং বক্স ভিসিবল হবে
    visible("timeremaining");

    let time = 60;
    let timeRemening = setInterval(function() {
      time -= 1;
      timego.innerHTML = time;
      if (time == 0) {
        clearInterval(timeRemening);
        document.getElementById("gameover").innerHTML =
          "<p>GAME IS OVER !</p><p> YOUR SCORE IS " + score + " </p>";
        visible("gameover");
      }
    }, 1000);

    // রেনডমলি একটি প্রশ্ন এবং মাল্টিপল উত্তর সো হবে
    QandA();
  } else {
    // যদি বাটনের ভিতর Start Game লেখা না থাকে তাহলে
    // পেই্জটি রিলোড হবে
    location.reload();
  };
};

// যদি গেইম চালু থাকে
if (btn.innerHTML != "Start Game") {
  checkAnswer();
};

// একটি ফাংশন বানিয়ে নিবো যার কাজ হবে আরগুমেন্টে দেওয়া আইডিটীর ভিসিবিলিটি চেইন্জ করে ভিসিবল করে দেওয়া ।
function visible(idname) {
  document.getElementById(idname).style.visibility = "visible";
};
// একটি ফাংশন বানিয়ে নিবো যার কাজ হবে আরগুমেন্টে দেওয়া আইডিটীর ভিসিবিলিটি চেইন্জ করে হিডেন করে দেওয়া ।
function hidden(idname) {
  document.getElementById(idname).style.visibility = "hidden";
};

function QandA() {
  // একটি রেনডম নাম্বার বানিয়ে নিবো
  let randomNumberOne = Math.floor(Math.random() * 10) + 1;
  // আরেকটি রেনডম নাম্বার বানিয়ে নিবো
  let randomNumberTwo = Math.floor(Math.random() * 10) + 1;

  // এবার রেনডম নাম্বারদুটিকে নিয়ে একটি প্রশ্ন বানিয়ে প্রশ্নটি সো করাবো
  document.getElementById("question").innerHTML =
    "<p>" + randomNumberOne + " &#215; " + randomNumberTwo + "</p>";

  // সঠীক উত্তরটি স্টোর করে রাখবো ।
  correctAnswer = randomNumberOne * randomNumberTwo;

  let CorrectAnswerBox = Math.floor(Math.random() * 4) + 1;

  document.getElementById("box" + CorrectAnswerBox).innerHTML = correctAnswer;

  // আরো তিনটি ভুল উত্তর বানিয়ে নিবো
  for (i = 1; i < 5; i++) {
    let wrongAnswer;
    // প্রথমেই একটি wrongAnswer বানিয়ে নিবে
    do {
      let a = Math.floor(Math.random() * 10) + 1;
      let b = Math.floor(Math.random() * 10) + 1;
	  wrongAnswer = a * b;

      // wrongAnswer এবং correctAnswer এর মান একি হলে সে আরেকটি নতুন wrongAnswer বানিয়ে নিবে এবং আরেকটি শর্ত, বানানো wrongAnswer টি যদি পূর্বে বানানো wrongAnswer এর মানের সমান হয় তাহলে আরেকটি wrongAnswer বানায়ে নিবে ।
    } while (wrongAnswer == correctAnswer && wrongAnswer == wrongAnswer);

    // CorrectAnswerBox এবং i এর মান সমান না হলে
    if (CorrectAnswerBox != i) {
      document.getElementById("box" + i).innerHTML = wrongAnswer;
    };
  };
};

// প্রতিটি answerBox এ addEventListner দিয়ে একটি ক্লিক ইভেন্ট যোগ করে নিব ।
for (i = 1; i < 5; i++) {
  document.getElementById("box" + i).addEventListener("click", checkAnswer);
};

// একটি ফাংশন বানিয়ে নিবো যার কাজ হবে উত্তর সঠীক কিনা সেটা যাচাই করার পর right বক্স কে ভিসিবল করবে নাকি wrong বক্সকে ভিসিবল করবে ।
function checkAnswer() {
  // ক্লিককৃত উত্তরটি যদি সঠীক হয়
  if (this.innerHTML == correctAnswer) {
    // স্কোর ১ বেড়ে যাবে
    score += 1;

    // স্কোর বক্সে নতুন স্কোরটি দেখাবে
    scoreBox.innerHTML = score;

    // কারেক্ট বক্স ১ সেকেন্ড এর জন্য ভিসিবল হবে
    visible("right");
    setTimeout(function() {
      hidden("right");
    }, 1000);

    // রেনডমলি একটি প্রশ্ন এবং মাল্টিপল উত্তর সো হবে
    QandA();
  } else {
    // ক্লিককৃত উত্তরটি ভুল হলে

    // ট্রাই এ্যাগেইন বক্সকে ১ সেকেন্ড এর জন্য ভিসিবল হবে
    visible("wrong");
    setTimeout(function() {
      hidden("wrong");
    }, 1000);
  };
};
