$(document).ready(function () {
    let startTime;
    const sentence = [
        "Design is not just what it looks like and how it feels. Design is how it works.",
        "You can't just ask customers what they want and then try to give that to them. By the time you get it built, they'll want something new.",
        "A quick brown fox jump over the lazy dog",
        "The javascript this keyword refer to the object it belongs to",
        "This keyword has differente values depending on where its used",
        "Alone, This refer to the Global Object",
        "In a regular function this refer to the Global Object",
        "In a method this refer to the owner Object",
        "Execution contex the environment which our code is executed and is evaluted",
        "Coding teaches us to find out that everything has a creator."
    ];

    // ইউজার যখন submit বাটনে ক্লিক করবে ।
    $("#btn").click(function () {
        // greeting থেকে ইউজারের নামটি নিয়ে স্টোর করবো ।
        let userName = $("#greeting").val();

        if(userName.length>0){
            
            $("#UserName").hide();
            $("#heading1,#paragraph,#textarea,#btn1").show();
            $("#heading1>span").text(userName).addClass("capitalize");
        };

        // ইউজার যখন start বাটনে ক্লিক করবে ।
        $("#btn1").click(function () {

            let buttonText = $(this).text();
            if(buttonText=="Start"){
                // চেইঞ্জ start বাটন to Done
                $(this).text("Done");
                $("#textarea").val("").focus();

                // একটি রেন্ডম নাম্বার বানিয়ে নিবো ।
                let randomNumber = Math.floor(Math.random()*sentence.length);
                $("#paragraph").text(sentence[randomNumber]);
                // টাইপকরার শুরুর সময়
                startTime = new Date($.now());
                
            }else{
                // চেইঞ্জ Done বাটন to Start
                $(this).text("Start");
                // textarea থেকে ইউজারের টাইপকৃত বাক্যটি নিব ।
                let totalWords = $("#textarea").val().split(" ").length;
                // টাইপকরার শেষ সময়
                let endTime = new Date($.now());
                let spendTime = Math.floor((endTime-startTime)/1000);
                let typingSpeed = speed(totalWords,spendTime);
                let randomSentence = $("#paragraph").text();
                let userSentence = $("#textarea").val();
                let correctWord = compareWords(randomSentence,userSentence);
                let sentenceLength = randomSentence.split(" ").length;
                let wrongWord = (sentenceLength-correctWord);
                showResult(typingSpeed,correctWord,wrongWord);
            };
        });
    });

    /* টাইপকৃত মোট শব্দের সংখ্যা এবং ব্যায়কৃত সময় দিয়ে,প্রতি মিনিটে টাইপিং স্পিড বের করবো । */
    function speed(totalWords,spendTime) {
        return Math.floor((totalWords/spendTime)*60);
    };

    function compareWords(randomSentence,userSentence) {
        let a = randomSentence.split(" ");
        let b = userSentence.split(" ");
        let correctWords = 0;
        
        // for(let i = 0; a.length>i; i++){
        //     if(a[i] == b[i]){
        //         correctWords+=1;
        //     };
        // };

        a.forEach(function (value,index){
            if(a[index] == b[index]){
                correctWords+=1;
            };
        });
        return correctWords;
    };

    function showResult(speed,correct,wrong) {
        if(correct>0){
            $("#paragraph").text("You can type "+speed+" words per minute and you are write "+correct+" correct words and "+wrong+" wrong words");
        }else{
            $("#paragraph").text("You are type wrong words! please try again!");
        };
    };
});
