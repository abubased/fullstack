let userName;
let startTime;
const sentence = [
    "Design is not just what it looks like and how it feels. Design is how it works.",
    "You can't just ask customers what they want and then try to give that to them. By the time you get it built, they'll want something new.",
    "A quick brown fox jump over the lazy dog",
    "The javascript this keyword refer to the object it belongs to",
    "This keyword has differente values depending on where its used",
    "Alone, This refer to the Global Object",
    "In a regular function this refer to the Global Object",
    "In a method this refer to the owner Object",
    "Execution contex the environment which our code is executed and is evaluted",
    "Coding teaches us to find out that everything has a creator."
];

$("#button_1").click(function () {
    let name = $("#container_1>input").val();
    if(name.length > 0){
        userName = name.charAt(0).toUpperCase()+name.slice(1).toLowerCase();
        $("#container_1").hide();
        $("#container_2").show();
        $("#heading_2>span").text(userName);
    };

});

$("#button_2").click(function () {
    if($(this).text()=="Start"){
        $(this).text("Done");
        let randomNumber = Math.floor(Math.random()*sentence.length);
        $("#container_2>h3").text(sentence[randomNumber]);
        $("#user_string").val("").focus();
        startTime = new Date($.now());
        
    }else{
        $(this).text("Start");

        let endTime = new Date($.now());
        let spendTime = Math.floor((endTime-startTime)/1000);
       
        let userString = $("#user_string").val();
        let totalWords = userString.split(' ').length;

        let speed = typingSpeed(totalWords,spendTime);
        
        let randomSentence = $("#container_2>h3").text();

        let words = compareWords(userString,randomSentence);
        if(words['correctWords']>0){
            $("#container_2>h3").text("You can type "+speed+" words per minute. and you are type "+words['correctWords']+" correct words & "+words['wrongwords']+" wrong words");
        };

    };

});

function typingSpeed(a,b) {
    return Math.floor((a/b)*60);
};

function compareWords(x,y) {
    x = x.split(" ");
    y = y.split(" ");
    let correctWords = 0;
    let wrongwords = 0;
    for(let i = 0; i<y.length; i++){
        if(y[i]==x[i]){
            correctWords+=1;
        }else{
            wrongwords+=1;
        };
    };
    return {correctWords,wrongwords};
};
